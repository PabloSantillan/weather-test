package com.mercadolibre.weather.job;

import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mercadolibre.weather.service.WeatherService;

@Component
public class RefreshWeather {

    private static final Logger LOGGER = LoggerFactory.getLogger(RefreshWeather.class);
    private WeatherService weatherService;
    
    @Autowired
    public RefreshWeather(WeatherService weatherService) {
    	this.weatherService = weatherService;
    }

    @Scheduled(cron = "0 0 1 * * ?")
    public void run() throws UnknownHostException  {
        LOGGER.info("Running process: refresh weather");        
        weatherService.calculateWeather();
    }  

}
