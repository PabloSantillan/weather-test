package com.mercadolibre.weather.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.weather.model.Point;

@Service
public class PositionService {
	
	private static int FULL_ANGLE = 360;
	private final ConfigurationService configurationService;	
	
	@Autowired
	public PositionService(ConfigurationService configurationService){
		this.configurationService = configurationService;
	}
	
	public Point getAnglePosition(double radius, double currentAngle, double degress){
		double newAngle = getAngleAfterMove(currentAngle, degress);
		
		double x = radius * Math.cos(Math.toRadians(newAngle));
		double y = radius * Math.sin(Math.toRadians(newAngle));
		
		return Point.from(x, y);
	}

	public double getAngleAfterMove(double currentAngle, double degress){
		double newAngle = (currentAngle - degress) % FULL_ANGLE;
		
		if (newAngle < 0){
			newAngle += FULL_ANGLE;
		}
		
		return newAngle;
	}
	
	public boolean areAligned(Point a, Point b, Point c){
		
		double slopeAB = getSlope(a, b);
		double slopeBC = getSlope(b, c);
		
		if (Double.isInfinite(slopeAB) || Double.isInfinite(slopeBC)){
			return compare(a.getX(), c.getX()) == 0;
		} else {
			return compare(slopeAB, slopeBC) == 0;
		}
	}
	
	public boolean areAlignedToOrigin(Point a, Point b, Point c){
		double slopeAB = getSlope(a, b);
		double slopeBC = getSlope(b, c);
				
		if (compare(a.getX(), b.getX()) == 0  || compare(b.getX(), c.getX()) == 0){
			return compare(a.getX(), c.getX()) == 0 && compare(a.getX(), 0) == 0;
		} else {
			boolean alignedToOrigin = compare(slopeAB, slopeBC) == 0 && compare(a.getY(), a.getX() * slopeAB) == 0;
			if (alignedToOrigin){
				return true;
			} else {
				return false;
			}
		}
	}
		
	public boolean isPointInsideTriangle(Point v1, Point v2, Point v3, Point p){
		double v1v2v3Orientation = getOrientation(v1, v2, v3);
		double v1v2pOrientation = getOrientation(v1, v2, p);
		double v2v3pOrientation = getOrientation(v2, v3, p);
		double v3v1pOrientation = getOrientation(v3, v1, p);
		
		return (v1v2v3Orientation > 0 && v1v2pOrientation > 0 && v2v3pOrientation > 0 && v3v1pOrientation > 0) ||
			   (v1v2v3Orientation < 0 && v1v2pOrientation < 0 && v2v3pOrientation < 0 && v3v1pOrientation < 0);
	}
		
	public double maxPerimeterByRadius(double ra, double rb, double rc){
		double denominator = (Math.pow(ra*rb, 2)) + (Math.pow(rb*rc, 2)) + (Math.pow(ra*rc, 2));
		double division = (double)3 / denominator;
		double sqrtPow = Math.sqrt(Math.pow(division, 3));
		
		double arc = Math.acos(Math.pow(ra*rb*rc, 2) * sqrtPow);

		double r = (Math.sqrt(division) * ra*rb*rc) / (2 * Math.cos(arc / 3));
				
		double maxPerimeter = 2 * (sqrtOfSubstraction(ra, r) + sqrtOfSubstraction(rb, r) + sqrtOfSubstraction(rc, r));
		return round(maxPerimeter);
	}
	
	public double getPerimeter(Point a, Point b, Point c) {
		double dAB = Math.hypot(b.getX() - a.getX(), b.getY() - a.getY());
		double dBC = Math.hypot(c.getX() - b.getX(), c.getY() - b.getY());
		double dCA = Math.hypot(a.getX() - c.getX(), a.getY() - c.getY());
		double sum = dAB + dBC + dCA;
		
		return round(sum);
	}

	public int compare(double a, double b){

		double ar = round(a);
		double br = round(b);

		return Double.compare(ar, br);
	}

	public double round(double a){
		try {			
			return Double.isNaN(a) ? a : BigDecimal.valueOf(a).setScale(configurationService.get().getDecimals(), RoundingMode.HALF_UP).doubleValue();
		} catch (Exception e) {
			System.out.println("excepcion redoundeando valor: " +a);
			throw e;
		}
	}

	// private function(s)
	
	private double getOrientation(Point a, Point b, Point c){
		return ((a.getX() - c.getX()) * (b.getY() - c.getY())) - ((a.getY() - c.getY()) * (b.getX() - c.getX()));
	}
	
	private double getSlope(Point a, Point b){
		return (b.getY() - a.getY()) / (b.getX() - a.getX());
	}
	
	private double sqrtOfSubstraction(double a, double b){
		return Math.sqrt((Math.pow(a, 2) - Math.pow(b, 2)));
	}	
}
