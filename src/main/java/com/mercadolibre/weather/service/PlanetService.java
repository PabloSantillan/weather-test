package com.mercadolibre.weather.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.weather.aspect.annotation.Reloadable;
import com.mercadolibre.weather.exception.NotFoundException;
import com.mercadolibre.weather.exception.status.NotFoundStatus;
import com.mercadolibre.weather.model.Planet;
import com.mercadolibre.weather.repository.PlanetRepository;

@Service
public class PlanetService {
	
	private final PlanetRepository repository;
	
	@Autowired
	public PlanetService(PlanetRepository planetRepository){
		this.repository = planetRepository;
	}
			
	public List<Planet> getPlanets(){
		return repository.getPlanets();
	}
	
	public Planet getBetasoide(){
		return repository.getBetasoide();
	}

	public Planet getFarengi(){
		return repository.getFarengi();
	}

	public Planet getVulcano(){
		return repository.getVulcano();
	}

	@Reloadable
	public void update(List<Planet> planets){
		planets.forEach(planetToUpdate -> {
			Planet planet = repository.getByName(planetToUpdate.getName());
			
			if (planet == null){
				throw new NotFoundException(NotFoundStatus.PLANET_NOT_FOUND);
			}
			
			planet.setAngularVelocity(planetToUpdate.getAngularVelocity());
			planet.setSunDistance(planetToUpdate.getSunDistance());
			planet.setCurrentAngle(planetToUpdate.getCurrentAngle());

			repository.save(planet);					
		});
	}

	@Reloadable
	public void reset() {
		repository.reset();
	}
}
