package com.mercadolibre.weather.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.weather.exception.NotFoundException;
import com.mercadolibre.weather.exception.status.NotFoundStatus;
import com.mercadolibre.weather.model.Planet;
import com.mercadolibre.weather.model.Point;
import com.mercadolibre.weather.model.Weather;
import com.mercadolibre.weather.model.WeatherType;
import com.mercadolibre.weather.repository.WeatherRepository;
import com.mercadolibre.weather.service.weather.analyzer.WeatherAnalyzer;

@Service
public class WeatherService {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherService.class);
	
	private final PositionService positionService;
	private final ConfigurationService configurationService;
	private final PlanetService planetService;
	private final WeatherRepository weatherRepository;
	private final WeatherAnalyzer weatherAnalyzer;

	@Autowired
	public WeatherService(PositionService positionService, 
			ConfigurationService configurationService,
			PlanetService planetService,
			WeatherRepository weatherRepository,
			WeatherAnalyzer weatherAnalyzer){
		this.positionService = positionService;
		this.configurationService = configurationService;
		this.planetService = planetService;
		this.weatherRepository = weatherRepository;
		this.weatherAnalyzer = weatherAnalyzer;
	}
	
	public List<Weather> getWeather() {
		return weatherRepository.getWeather();
	}

	public List<Weather> getWeatherByType(WeatherType type) {
		return weatherRepository.getWeatherByType(type);
	}

	public List<Weather> getWeatherByDay(Integer day) {
		List<Weather> dayWeather = weatherRepository.getWeatherByDay(day);
		if (dayWeather.isEmpty()){
			throw new NotFoundException(NotFoundStatus.DAY_WEATHER_NOT_FOUND);
		}
		return dayWeather;
	}

	public void calculateWeather(){
		
		Planet vulcano = planetService.getVulcano();
		Planet farengi = planetService.getFarengi();
		Planet betasoide = planetService.getBetasoide();
		
		long days = ChronoUnit.DAYS.between(LocalDate.now(), LocalDate.now().plusYears(configurationService.get().getYears()));
		
		List<Weather> weather = new ArrayList<Weather>();
		
		for (int i = 1; i <= days; i++) {
			Point vulcanoPoint = this.positionService.getAnglePosition(vulcano.getSunDistance(), vulcano.getCurrentAngle(), vulcano.getAngularVelocity() * i);
			Point farengiPoint = this.positionService.getAnglePosition(farengi.getSunDistance(), farengi.getCurrentAngle(), farengi.getAngularVelocity() * i);
			Point betasoidePoint = this.positionService.getAnglePosition(betasoide.getSunDistance(), betasoide.getCurrentAngle(), betasoide.getAngularVelocity() * i);
			
			WeatherType type = weatherAnalyzer.analyze(vulcanoPoint, farengiPoint, betasoidePoint);
			Weather dayWeather = Weather.from(i, type, getPositions(vulcanoPoint, farengiPoint, betasoidePoint));
			LOGGER.debug(dayWeather.toString());
			weather.add(dayWeather);			
		}

		weatherRepository.save(weather);
	}

	// private function(s)
	
	private String getPositions(Point vp, Point bp, Point fp){
		Integer decimals = configurationService.get().getDecimals();
		return "(" + vp.printRoundedPoints(decimals) + ")(" + bp.printRoundedPoints(decimals) + ")(" +  fp.printRoundedPoints(decimals) + ")";				
	}
}
