package com.mercadolibre.weather.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.weather.aspect.annotation.Reloadable;
import com.mercadolibre.weather.exception.NotFoundException;
import com.mercadolibre.weather.exception.status.NotFoundStatus;
import com.mercadolibre.weather.model.Configuration;
import com.mercadolibre.weather.repository.ConfigurationRepository;

@Service
public class ConfigurationService {
	
	private final ConfigurationRepository repository;

	@Autowired
	public ConfigurationService(ConfigurationRepository repository){
		this.repository = repository;
	}
	
	public Configuration get(){
		return repository.get();
	}
	
	@Reloadable
	public void update(Configuration newConfiguration){	
		Configuration configuration = repository.get();
		
		if (configuration == null){
			throw new NotFoundException(NotFoundStatus.CONFIGURATION_NOT_FOUND);
		}

		configuration.setDecimals(newConfiguration.getDecimals());
		configuration.setYears(newConfiguration.getYears());

		repository.save(configuration);
	}

	@Reloadable
	public void reset() {
		repository.reset();
	}
}
