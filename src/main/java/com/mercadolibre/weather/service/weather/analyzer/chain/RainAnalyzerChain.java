package com.mercadolibre.weather.service.weather.analyzer.chain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mercadolibre.weather.model.Point;
import com.mercadolibre.weather.model.WeatherType;
import com.mercadolibre.weather.service.PlanetService;
import com.mercadolibre.weather.service.PositionService;

@Component
public class RainAnalyzerChain extends AbstractAnalyzerChain {
    private static final Logger LOGGER = LoggerFactory.getLogger(RainAnalyzerChain.class);
    private final PlanetService planetService;
	
	@Autowired
	public RainAnalyzerChain(PositionService positionService, PlanetService planetService) {
		super(positionService);
		this.planetService = planetService;
	}
	
	@Override
	public WeatherType analyze(Point a, Point b, Point c) {
		if (positionService.isPointInsideTriangle(a, b, c, Point.from(0, 0))){

			double maxPerimeter = positionService.maxPerimeterByRadius(
					planetService.getVulcano().getSunDistance(), 
					planetService.getFarengi().getSunDistance(), 
					planetService.getBetasoide().getSunDistance());
						
			double currentPerimeter = positionService.getPerimeter(a, b, c);
			
			double diff = positionService.round(maxPerimeter - currentPerimeter);
			if (diff < 0.1){
				LOGGER.info("maxPer: " + maxPerimeter + " . CurrPer:" + currentPerimeter + ". Diff: " + diff);				
			} 

			if (positionService.compare(maxPerimeter, currentPerimeter) == 0){
				return WeatherType.HEAVY_RAIN;
			} 
						
			return WeatherType.RAIN;
		} 
		
		return next.analyze(a, b, c);
	}
}
