package com.mercadolibre.weather.service.weather.analyzer.chain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mercadolibre.weather.model.Point;
import com.mercadolibre.weather.model.WeatherType;
import com.mercadolibre.weather.service.PositionService;

@Component
public abstract class AbstractAnalyzerChain implements AnalyzerChain {
	protected AnalyzerChain next;
	protected final PositionService positionService;

	@Autowired
	public AbstractAnalyzerChain(PositionService positionService) {
		this.positionService = positionService;
	}
	
	@Override
	public void setNext(AnalyzerChain next){
		this.next = next;
	}

	@Override
	public abstract WeatherType analyze(Point a, Point b, Point c);
}
