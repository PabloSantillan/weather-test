package com.mercadolibre.weather.service.weather.analyzer.chain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mercadolibre.weather.model.Point;
import com.mercadolibre.weather.model.WeatherType;
import com.mercadolibre.weather.service.PositionService;

@Component
public class DroughtAnalyzerChain extends AbstractAnalyzerChain {

	@Autowired
	public DroughtAnalyzerChain(PositionService positionService) {
		super(positionService);
	}

	@Override
	public WeatherType analyze(Point a, Point b, Point c) {
		if (positionService.areAlignedToOrigin(a, b, c)){
			return WeatherType.DROUGHT;
		} 
		
		return next.analyze(a, b, c);			
	}
}
