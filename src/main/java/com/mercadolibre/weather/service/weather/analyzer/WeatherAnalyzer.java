package com.mercadolibre.weather.service.weather.analyzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mercadolibre.weather.model.Point;
import com.mercadolibre.weather.model.WeatherType;
import com.mercadolibre.weather.service.weather.analyzer.chain.AnalyzerChain;
import com.mercadolibre.weather.service.weather.analyzer.chain.DroughtAnalyzerChain;
import com.mercadolibre.weather.service.weather.analyzer.chain.OptimalConditionAnalyzerChain;
import com.mercadolibre.weather.service.weather.analyzer.chain.RainAnalyzerChain;
import com.mercadolibre.weather.service.weather.analyzer.chain.UnknownAnalyzerChain;

@Component
public class WeatherAnalyzer {
	
	private AnalyzerChain initialAnalyzer;
	
	@Autowired
	public WeatherAnalyzer(UnknownAnalyzerChain unknownAnalyzer,
			RainAnalyzerChain rainAnalyzer,
			OptimalConditionAnalyzerChain optimalAnalyzer,
			DroughtAnalyzerChain droughtAnalyzer){
		
		this.initialAnalyzer = droughtAnalyzer;
		droughtAnalyzer.setNext(optimalAnalyzer);
		optimalAnalyzer.setNext(rainAnalyzer);
		rainAnalyzer.setNext(unknownAnalyzer);
	}
	
	public WeatherType analyze(Point a, Point b, Point c){
		return this.initialAnalyzer.analyze(a, b, c);
	}
}
