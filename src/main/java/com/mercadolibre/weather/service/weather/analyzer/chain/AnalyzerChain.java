package com.mercadolibre.weather.service.weather.analyzer.chain;

import com.mercadolibre.weather.model.Point;
import com.mercadolibre.weather.model.WeatherType;

public interface AnalyzerChain {
	
	void setNext(AnalyzerChain next);
	WeatherType analyze(Point a, Point b, Point c);
}
