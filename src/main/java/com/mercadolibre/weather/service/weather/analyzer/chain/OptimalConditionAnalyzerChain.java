package com.mercadolibre.weather.service.weather.analyzer.chain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mercadolibre.weather.model.Point;
import com.mercadolibre.weather.model.WeatherType;
import com.mercadolibre.weather.service.PositionService;

@Component
public class OptimalConditionAnalyzerChain extends AbstractAnalyzerChain {

	@Autowired
	public OptimalConditionAnalyzerChain(PositionService positionService) {
		super(positionService);
	}

	@Override
	public WeatherType analyze(Point a, Point b, Point c) {
		if (positionService.areAligned(a, b, c)){
			return WeatherType.OPTIMAL_CONDITION;
		} 
		
		return next.analyze(a, b, c);
	}
}
