package com.mercadolibre.weather.model;

public class Weather {
	private Integer day;
	private WeatherType type;
	private String positions;
	
	public Weather(){
	}
	
	private Weather(Integer day, WeatherType type, String positions){
		this.day = day;
		this.type = type;
		this.positions = positions;
	}
	
	public static Weather from(Integer day, WeatherType type, String positions){
		return new Weather(day, type, positions);
	}
	
	@Override
	public String toString() {
		return "Weather [day=" + day + ", type=" + type + ", positions=" + positions + "]";
	}
	
	// getters and setters
	
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public WeatherType getType() {
		return type;
	}
	public void setType(WeatherType type) {
		this.type = type;
	}
	public String getPositions() {
		return positions;
	}
	public void setPositions(String positions) {
		this.positions = positions;
	}
}
