package com.mercadolibre.weather.model;

public class Planet {
	private String name;
	private double sunDistance;
	private double angularVelocity;
	private double currentAngle;
	
	public Planet(){
		
	}
	
	public Planet(String name, double sunDistance, double angularVelocity, double currentAngle){
		this.name = name;
		this.sunDistance = sunDistance;
		this.angularVelocity = angularVelocity;
		this.currentAngle = currentAngle;
	}
	
	// getters and setters
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSunDistance() {
		return sunDistance;
	}
	public void setSunDistance(double sunDistance) {
		this.sunDistance = sunDistance;
	}
	public double getAngularVelocity() {
		return angularVelocity;
	}
	public void setAngularVelocity(double angularVelocity) {
		this.angularVelocity = angularVelocity;
	}
	public double getCurrentAngle() {
		return currentAngle;
	}
	public void setCurrentAngle(double currentAngle) {
		this.currentAngle = currentAngle;
	}
}
