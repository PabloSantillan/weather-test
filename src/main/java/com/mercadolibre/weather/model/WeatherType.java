package com.mercadolibre.weather.model;

public enum WeatherType {
	DROUGHT("Drought"),
	RAIN("Rain"),
	HEAVY_RAIN("Heavy rain"),
	OPTIMAL_CONDITION("Optimal conditions of pressure and temperature"),
	UNKNOWN("Weather unknown");
	
	private String description;
	
	private WeatherType(String description){
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
