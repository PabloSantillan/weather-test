package com.mercadolibre.weather.model;

public class Configuration {
	private Integer decimals = 10;
	private Integer years = 10;
	
	@Override
	public String toString() {
		return "Configuration [decimals=" + decimals + ", years=" + years + "]";
	}

	// getters and setters
	
	public Integer getDecimals() {
		return decimals;
	}
	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}
	public Integer getYears() {
		return years;
	}
	public void setYears(Integer years) {
		this.years = years;
	}
}
