package com.mercadolibre.weather.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Point {
	
	private double x;
	private double y;
	
	private Point(){}
	
	private Point(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public static Point from(double x, double y){
		Point point = new Point(x, y);
		return point;
	}
	
	public Point roundedTo(int decimals){
		double roundedX = new BigDecimal(this.x).setScale(decimals, RoundingMode.HALF_UP).doubleValue();
		double roundedY = new BigDecimal(this.y).setScale(decimals, RoundingMode.HALF_UP).doubleValue();
		
		return Point.from(roundedX, roundedY);
	}
	
	public String printRoundedPoints(int decimals){
		double xr = new BigDecimal(x).setScale(decimals, RoundingMode.HALF_UP).doubleValue();
		double yr = new BigDecimal(y).setScale(decimals, RoundingMode.HALF_UP).doubleValue();

		return xr + "," + yr;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";		
	}

	// getters and setters

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}
