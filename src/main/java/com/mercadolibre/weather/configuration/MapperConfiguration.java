package com.mercadolibre.weather.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mercadolibre.weather.controller.dto.WeatherDto;
import com.mercadolibre.weather.model.Weather;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Configuration
public class MapperConfiguration {

    private MapperFactory mapperFactory;

    public MapperConfiguration() {
        this.mapperFactory = new DefaultMapperFactory.Builder().build();
    }
    
    @Bean
    public MapperFacade mapperFacade() {
        
    	mapperFactory.classMap(WeatherDto.class, Weather.class)
    	.customize(
	    		new CustomMapper<WeatherDto, Weather>() {
	 			   @Override
	 			   public void mapBtoA(Weather b, WeatherDto a, MappingContext context) {
	 				   super.mapBtoA(b, a, context);
	 				   a.setWeather(b.getType().getDescription());
	 			   }
	 			 })
    	.byDefault()
    	.register();
    	
        return mapperFactory.getMapperFacade();
    }
}