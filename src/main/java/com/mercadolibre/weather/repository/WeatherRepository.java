package com.mercadolibre.weather.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.mercadolibre.weather.model.Weather;
import com.mercadolibre.weather.model.WeatherType;

@Repository
public class WeatherRepository {
	
	private List<Weather> weather = new ArrayList<Weather>();

	
	public List<Weather> getWeather(){
		return this.weather;
	}
	
	public List<Weather> getWeatherByType(WeatherType type) {
		return this.weather.stream().filter(w -> w.getType().equals(type)).collect(Collectors.toList());
	}

	public List<Weather> getWeatherByDay(Integer day){
		return weather.stream().filter(w -> w.getDay().equals(day)).collect(Collectors.toList());
	}

	public void clear() {
		this.weather.clear();
	}
	
	public void save(List<Weather> weather){
		this.weather.clear();
		this.weather.addAll(weather);
	}
}
