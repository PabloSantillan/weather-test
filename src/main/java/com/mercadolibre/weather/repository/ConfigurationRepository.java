package com.mercadolibre.weather.repository;

import org.springframework.stereotype.Repository;

import com.mercadolibre.weather.model.Configuration;

@Repository
public class ConfigurationRepository {
	
	private Configuration configuration;
	
	public ConfigurationRepository(){
		this.configuration = new Configuration();
	}
	
	public Configuration get() {
		return configuration;
	}

	public void save(Configuration newConfiguration) {
		// save to some database ...
	}

	public void reset() {
		this.configuration = new Configuration();
	}
}
