package com.mercadolibre.weather.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.mercadolibre.weather.model.Planet;

@Repository
public class PlanetRepository {
	
	private Planet vulcano;
	private Planet betasoide;
	private Planet farengi;
	
	private List<Planet> planets;
	
	public PlanetRepository(){
		reset();
	}
	
	public List<Planet> getPlanets(){
		return this.planets;
	}
	
	public Planet getBetasoide(){
		return this.betasoide;
	}

	public Planet getFarengi(){
		return this.farengi;
	}

	public Planet getVulcano(){
		return this.vulcano;
	}
	
	public Planet getByName(String name){
		Optional<Planet> planet = this.planets.stream().filter(p -> p.getName().equals(name)).findFirst();
		
		return planet.isPresent() ? planet.get() : null;
	}

	public void save(Planet planet) {
		// save to some database...
	}

	public void reset() {
		this.farengi =   new Planet("Farengi"  , 5,  1, 90);
		this.vulcano =   new Planet("Vulcano"  , 10,-5, 90);
		this.betasoide = new Planet("Betasoide", 20, 3, 90);
		
		this.planets = new ArrayList<Planet>();
		this.planets.add(farengi);
		this.planets.add(vulcano);
		this.planets.add(betasoide);
	}
}
