package com.mercadolibre.weather.controller.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.mercadolibre.weather.exception.BadRequestException;
import com.mercadolibre.weather.exception.NotFoundException;

@ControllerAdvice
public class ExceptionHandlerAdvice {

	private final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);
	
	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<Error> handleBadRequestException(BadRequestException ex) {

		LOGGER.error(ex.getMessage(), ex);		
		Error error = new Error(ex.getCode(), ex.getMessage());
		
		return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Error> handleNotFoundException(NotFoundException ex) {

		LOGGER.error(ex.getMessage(), ex);		
		Error error = new Error(ex.getCode(), ex.getMessage());
		
		return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
	}		
}
