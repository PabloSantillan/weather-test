package com.mercadolibre.weather.controller.dto;

public class ConfigurationDto {
	
	private Integer decimals;
	private Integer years;
	
	public ConfigurationDto(){	
	}
	
	public ConfigurationDto(Integer decimals, Integer years){
		this.decimals = decimals;
		this.years = years;
	}
	
	@Override
	public String toString() {
		return "ConfigurationDto [decimals=" + decimals + ", years=" + years + "]";
	}

	// getters and setters
	
	public Integer getDecimals() {
		return decimals;
	}
	public void setDecimals(Integer decimals) {
		this.decimals = decimals;
	}
	public Integer getYears() {
		return years;
	}
	public void setYears(Integer years) {
		this.years = years;
	}
}
