package com.mercadolibre.weather.controller.dto;

public class WeatherDto {
	
	private Integer day;
	private String weather;
	private String positions;
	
	public WeatherDto(){
		
	}
	
	public WeatherDto(Integer day, String weather, String positions){
		this.day = day;
		this.weather = weather;
		this.positions = positions;
	}

	@Override
	public String toString() {
		return "WeatherDto [day=" + day + ", weather=" + weather + ", positions=" + positions + "]";
	}
	
	// getters and seters
	
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public String getWeather() {
		return weather;
	}
	public void setWeather(String weather) {
		this.weather = weather;
	}

	public String getPositions() {
		return positions;
	}

	public void setPositions(String positions) {
		this.positions = positions;
	}
}
