package com.mercadolibre.weather.controller.dto;

public class WeatherCountDto {
	private String weather;
	private Integer count;
	
	public WeatherCountDto() {
	}
	
	private WeatherCountDto(String weather, Integer count){
		this.weather = weather;
		this.count = count;
	}
	
	public static WeatherCountDto from(String weather, Integer count){
		return new WeatherCountDto(weather, count);
	}
	
	@Override
	public String toString() {
		return "WeatherCountDto [weather=" + weather + ", count=" + count + "]";
	}
	
	// getters and setters
	
	public String getWeather() {
		return weather;
	}
	public void setWeather(String weather) {
		this.weather = weather;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}
