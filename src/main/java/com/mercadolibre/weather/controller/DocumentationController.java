package com.mercadolibre.weather.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("documentation")
@ApiIgnore
public class DocumentationController {

    @RequestMapping(method = RequestMethod.GET) 
    @ResponseBody
    public void redirectToDocumentation(HttpServletResponse response) {    	
    	try {
			response.sendRedirect("swagger-ui.html");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
