package com.mercadolibre.weather.controller.validator;

import org.springframework.stereotype.Component;

import com.mercadolibre.weather.exception.BadRequestException;
import com.mercadolibre.weather.exception.status.BadRequestStatus;
import com.mercadolibre.weather.model.Configuration;

@Component
public class ConfigurationValidator {

	public void validate(Configuration configuration){
		if (configuration.getDecimals() == null){
			throw new BadRequestException(BadRequestStatus.REQUIRED_FIELD, "decimals");
		}
		if (configuration.getDecimals() < 0 || configuration.getDecimals() > 20){
			throw new BadRequestException(BadRequestStatus.DECIMAL_INVALID_VALUE);
		}
		if (configuration.getYears() == null){
			throw new BadRequestException(BadRequestStatus.REQUIRED_FIELD, "years");
		}
		if (configuration.getYears() < 0 || configuration.getYears() > 40){
			throw new BadRequestException(BadRequestStatus.YEAR_INVALID_VALUE);
		}
	}
}
