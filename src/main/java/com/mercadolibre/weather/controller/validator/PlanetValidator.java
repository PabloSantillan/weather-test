package com.mercadolibre.weather.controller.validator;

import org.springframework.stereotype.Component;

import com.mercadolibre.weather.exception.BadRequestException;
import com.mercadolibre.weather.exception.status.BadRequestStatus;
import com.mercadolibre.weather.model.Planet;

@Component
public class PlanetValidator {

	public void validate(Planet planet){
		if (planet.getCurrentAngle() > 360 || planet.getCurrentAngle() < 0){
			throw new BadRequestException(BadRequestStatus.CURRENT_ANGLE_INVALID_VALUE);
		}
		if (planet.getSunDistance() <= 0){
			throw new BadRequestException(BadRequestStatus.SUN_DISTANCE_INVALID_VALUE);
		}
	}
}
