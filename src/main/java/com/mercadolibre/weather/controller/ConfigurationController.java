package com.mercadolibre.weather.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mercadolibre.weather.controller.documentation.ConfigurationResources;
import com.mercadolibre.weather.controller.dto.ConfigurationDto;
import com.mercadolibre.weather.controller.validator.ConfigurationValidator;
import com.mercadolibre.weather.mapper.Mapper;
import com.mercadolibre.weather.model.Configuration;
import com.mercadolibre.weather.service.ConfigurationService;

@RestController
@RequestMapping("configuration")
public class ConfigurationController implements ConfigurationResources {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationController.class);

    private final ConfigurationValidator configurationValidator; 
    private final ConfigurationService configurationService;
    private final Mapper mapper;
    
    @Autowired
    public ConfigurationController(ConfigurationValidator configurationValidator,
    		ConfigurationService configurationService, 
    		Mapper mapper){
    	this.configurationValidator = configurationValidator;
    	this.configurationService = configurationService;
    	this.mapper = mapper;
    }
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<ConfigurationDto> getConfiguration() {
        
        LOGGER.info("Get configuration");

        Configuration configuration = configurationService.get();
        ConfigurationDto response = mapper.map(configuration, ConfigurationDto.class);
        
        return new ResponseEntity<ConfigurationDto>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateConfiguration(@RequestBody ConfigurationDto newConfiguration) {
        
        LOGGER.info("update configuration: " + newConfiguration);

        Configuration configuration = mapper.map(newConfiguration, Configuration.class);
        configurationValidator.validate(configuration);
        configurationService.update(configuration);
        
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public ResponseEntity<String> reset() {
        
        LOGGER.info("reset changes");
      
        configurationService.reset();
        
        return new ResponseEntity<String>("Configuration was restored.", HttpStatus.CREATED);
    }
}
