package com.mercadolibre.weather.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mercadolibre.weather.controller.documentation.WeatherResources;
import com.mercadolibre.weather.controller.dto.WeatherCountDto;
import com.mercadolibre.weather.controller.dto.WeatherDto;
import com.mercadolibre.weather.mapper.Mapper;
import com.mercadolibre.weather.model.Weather;
import com.mercadolibre.weather.model.WeatherType;
import com.mercadolibre.weather.service.WeatherService;

@RestController
public class WeatherController implements WeatherResources{
    private static final Logger LOGGER = LoggerFactory.getLogger(WeatherController.class);

	private final WeatherService weatherService;
	private final Mapper mapper;
	
	@Autowired
	public WeatherController(WeatherService weatherService, Mapper mapper){
		this.weatherService = weatherService;
		this.mapper = mapper;
	}
	
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<WeatherDto>> getWeather(@RequestParam(required=false) Integer day, @RequestParam(required=false) WeatherType type) {
        
        LOGGER.info("Get weather. Day: " + day );

        List<Weather> weather;
        if (day != null ) {
        	weather = weatherService.getWeatherByDay(day);
        } else if (type != null){
        	weather = weatherService.getWeatherByType(type);        	        	
        } else {
        	weather = weatherService.getWeather();        	
        }

        List<WeatherDto> response = mapper.map(weather, WeatherDto.class);
        return new ResponseEntity<List<WeatherDto>>(response, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/types/count", method = RequestMethod.GET)
    public ResponseEntity<List<WeatherCountDto>> getWeatherTypesCount() {
        
        LOGGER.info("Get weather types count");
        List<WeatherCountDto> response = new ArrayList<WeatherCountDto>();
        
        List<Weather> weather = weatherService.getWeather();
        
        Map<WeatherType, List<Weather>> grouping = weather.stream().collect(Collectors.groupingBy(Weather::getType));
        grouping.forEach((type, list) -> {
        	response.add(WeatherCountDto.from(type.getDescription(), list.size()));
        });
        
        return new ResponseEntity<List<WeatherCountDto>>(response, HttpStatus.OK);
    }
}
