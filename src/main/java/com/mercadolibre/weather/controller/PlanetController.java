package com.mercadolibre.weather.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mercadolibre.weather.controller.documentation.PlanetResources;
import com.mercadolibre.weather.controller.dto.PlanetDto;
import com.mercadolibre.weather.controller.validator.PlanetValidator;
import com.mercadolibre.weather.mapper.Mapper;
import com.mercadolibre.weather.model.Planet;
import com.mercadolibre.weather.service.PlanetService;

@RestController
@RequestMapping("planets")
public class PlanetController implements PlanetResources {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlanetController.class);

    private final PlanetValidator planetValidator; 
    private final PlanetService planetService;
    private final Mapper mapper;
    
    @Autowired
    public PlanetController(PlanetValidator planetValidator,
    		PlanetService planetService, 
    		Mapper mapper){
    	this.planetValidator = planetValidator;
    	this.planetService = planetService;
    	this.mapper = mapper;
    }
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<PlanetDto>> getPlanets() {
        
        LOGGER.info("Get planets");

        List<Planet> planets = planetService.getPlanets();
        List<PlanetDto> response = mapper.map(planets, PlanetDto.class);
        
        return new ResponseEntity<List<PlanetDto>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateConfiguration(@RequestBody List<PlanetDto> planetsDto) {
        
        LOGGER.info("update planets: " + planetsDto);

        List<Planet> planets = mapper.map(planetsDto, Planet.class);
        planets.forEach(p -> {
        	planetValidator.validate(p);
        });
        
    	planetService.update(planets);
    	
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public ResponseEntity<String> reset() {
        
        LOGGER.info("reset planets configurations");
      
        planetService.reset();
        
        return new ResponseEntity<String>("Configuration was restored.", HttpStatus.CREATED);
    }
}
