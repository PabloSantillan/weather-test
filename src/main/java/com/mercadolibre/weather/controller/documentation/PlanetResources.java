package com.mercadolibre.weather.controller.documentation;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.mercadolibre.weather.controller.dto.PlanetDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="Planet", description = "Available planet operations")
public interface PlanetResources {

	@ApiOperation(value="Get planets", notes = "", response = PlanetDto.class, responseContainer= "List")
	ResponseEntity<List<PlanetDto>> getPlanets();
	
	@ApiOperation(value="Update planets", notes = "Name cannot be updated")
	ResponseEntity<Void> updateConfiguration(@RequestBody List<PlanetDto> planetsDto);
}
