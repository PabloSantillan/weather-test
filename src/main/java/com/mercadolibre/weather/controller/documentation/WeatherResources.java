package com.mercadolibre.weather.controller.documentation;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

import com.mercadolibre.weather.controller.dto.WeatherCountDto;
import com.mercadolibre.weather.controller.dto.WeatherDto;
import com.mercadolibre.weather.model.WeatherType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="Weather", description = "Available weather operations")
public interface WeatherResources {

	@ApiOperation(value="Get weather", notes = "", response = WeatherDto.class, responseContainer= "List")
    ResponseEntity<List<WeatherDto>> getWeather(@RequestParam(required=false) Integer day, @RequestParam(required=false) WeatherType type);
	
	@ApiOperation(value="Get weather count grouped by type", notes = "", response = WeatherCountDto.class, responseContainer= "List")
	ResponseEntity<List<WeatherCountDto>> getWeatherTypesCount();
}
