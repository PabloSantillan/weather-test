package com.mercadolibre.weather.controller.documentation;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.mercadolibre.weather.controller.dto.ConfigurationDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="Configuration", description = "Available configuration operations")
public interface ConfigurationResources {

	@ApiOperation(value="Get configuration", notes = "", response = ConfigurationDto.class)
	ResponseEntity<ConfigurationDto> getConfiguration();
	
	@ApiOperation(value="Update configuration", notes = "")
	ResponseEntity<Void> updateConfiguration(@RequestBody ConfigurationDto newConfiguration);
}
