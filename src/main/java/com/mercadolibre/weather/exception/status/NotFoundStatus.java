package com.mercadolibre.weather.exception.status;

public enum NotFoundStatus {
	
	DAY_WEATHER_NOT_FOUND(1, "Day weather not found"), 
	PLANET_NOT_FOUND(2, "Planet not found"), 
	CONFIGURATION_NOT_FOUND(3, "Configuration not found");
	
	private int code;
	private String message;
	
	private NotFoundStatus(int code, String message){
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
