package com.mercadolibre.weather.exception.status;

public enum BadRequestStatus {
	
	REQUIRED_FIELD(1, "{0} is a mandatory field"),
	DECIMAL_INVALID_VALUE(2, "decimal value must be between 0 and 20"),
	YEAR_INVALID_VALUE(3, "year value must be between 0 and 40"), 
	SUN_DISTANCE_INVALID_VALUE(4, "sun distance must by greater than 0"), 
	CURRENT_ANGLE_INVALID_VALUE(5, "current angle must be between 0 and 359");
	
	private int code;
	private String message;
	
	private BadRequestStatus(int code, String message){
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
