package com.mercadolibre.weather.exception;

public class MercadoLibreException extends RuntimeException {

	private static final long serialVersionUID = 2543324479239810700L;
	private int code;
	
	public MercadoLibreException(int code, String message){
		super(message);
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
