package com.mercadolibre.weather.exception;

import java.text.MessageFormat;

import com.mercadolibre.weather.exception.status.BadRequestStatus;

public class BadRequestException extends MercadoLibreException {

	private static final long serialVersionUID = -8513709604748048275L;

	public BadRequestException(BadRequestStatus status, Object... arguments){
		super(status.getCode(), MessageFormat.format(status.getMessage(), arguments));
	}	
}
