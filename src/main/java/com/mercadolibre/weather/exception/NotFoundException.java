package com.mercadolibre.weather.exception;

import com.mercadolibre.weather.exception.status.NotFoundStatus;

public class NotFoundException extends MercadoLibreException {

	private static final long serialVersionUID = 6873786847314263448L;

	public NotFoundException(NotFoundStatus status){
		super(status.getCode(), status.getMessage());
	}
}
