package com.mercadolibre.weather.mapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.MapperFacade;

@Component
public class Mapper {
    
    private MapperFacade mapperFacade;
    
    @Autowired
    public Mapper(MapperFacade mapperFacade) {
    	this.mapperFacade = mapperFacade;
    } 
    
    public <S,D> D map(S elem, Class<D> destinationClass){
    	return mapperFacade.map(elem, destinationClass);
    }

    public <S,D> List<D> map(List<S> list, Class<D> destinationClass){
    	return mapperFacade.mapAsList(list, destinationClass);
    }
}

