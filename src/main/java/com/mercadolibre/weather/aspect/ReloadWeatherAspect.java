package com.mercadolibre.weather.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.weather.aspect.annotation.Reloadable;
import com.mercadolibre.weather.service.WeatherService;

@Aspect
@Service
public class ReloadWeatherAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReloadWeatherAspect.class);

	private final WeatherService weatherService;
	
	@Autowired
	public ReloadWeatherAspect(WeatherService weatherService){
		this.weatherService = weatherService;
	}
	
	@After("@annotation(reloadable)")
	public void reload(JoinPoint pointcut, Reloadable reloadable){
		LOGGER.info("reload weather");
		
		weatherService.calculateWeather();
	}
}
