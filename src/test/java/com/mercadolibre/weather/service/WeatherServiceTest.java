package com.mercadolibre.weather.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.mercadolibre.weather.model.Configuration;
import com.mercadolibre.weather.model.Planet;
import com.mercadolibre.weather.model.Weather;
import com.mercadolibre.weather.model.WeatherType;
import com.mercadolibre.weather.repository.ConfigurationRepository;
import com.mercadolibre.weather.repository.PlanetRepository;
import com.mercadolibre.weather.repository.WeatherRepository;
import com.mercadolibre.weather.service.weather.analyzer.WeatherAnalyzer;
import com.mercadolibre.weather.service.weather.analyzer.chain.DroughtAnalyzerChain;
import com.mercadolibre.weather.service.weather.analyzer.chain.OptimalConditionAnalyzerChain;
import com.mercadolibre.weather.service.weather.analyzer.chain.RainAnalyzerChain;
import com.mercadolibre.weather.service.weather.analyzer.chain.UnknownAnalyzerChain;

public class WeatherServiceTest {

	private WeatherService weatherService;
	private ConfigurationService configurationService;
	private PlanetService planetService;
	private PositionService positionService;
	
	@Before
	public void setup(){		
		
		this.configurationService = new ConfigurationService(new ConfigurationRepository());
		this.planetService = new PlanetService(new PlanetRepository());
		this.positionService = new PositionService(configurationService);
		
		this.weatherService = new WeatherService(
				positionService, 
				configurationService, 
				planetService,
				new WeatherRepository(),
				getWeatherAnalyzer());
	}
	
	@Test
	public void testAllAlignedToSun(){
		Configuration configuration = configurationService.get();
		configuration.setYears(1);
		configurationService.update(configuration);
		
		List<Planet> planets = planetService.getPlanets();
		planets.forEach(p -> {
			p.setAngularVelocity(10d);
			p.setCurrentAngle(90d);
			
		});

		planetService.update(planets);

		weatherService.calculateWeather();
		
		List<Weather> weather = weatherService.getWeather();

		long days = ChronoUnit.DAYS.between(LocalDate.now(), LocalDate.now().plusYears(1));

		Assert.assertTrue("Days size error", days == weather.size());
		
		long count = weather.stream().filter(w -> w.getType() == WeatherType.DROUGHT).count();
		
		Assert.assertEquals("All day weather must be drought", days, count);
	}
	
	@Test
	public void testAllUnknown(){
		Configuration configuration = configurationService.get();
		configuration.setYears(1);
		configurationService.update(configuration);
		
		Planet betasoide = planetService.getBetasoide();
		Planet farengi = planetService.getFarengi();
		Planet vulcano = planetService.getVulcano();
		
		List<Planet> toUpdate = new ArrayList<>();
		toUpdate.add(farengi);
		toUpdate.add(betasoide);
		toUpdate.add(vulcano);
		
		betasoide.setCurrentAngle(90);
		betasoide.setAngularVelocity(10);

		farengi.setCurrentAngle(0);
		farengi.setAngularVelocity(10);

		vulcano.setCurrentAngle(45);
		vulcano.setAngularVelocity(10);

		planetService.update(toUpdate);
		weatherService.calculateWeather();
		
		List<Weather> weather = weatherService.getWeather();

		long days = ChronoUnit.DAYS.between(LocalDate.now(), LocalDate.now().plusYears(1));

		Assert.assertTrue("Days size error", days == weather.size());
		
		long count = weather.stream().filter(w -> w.getType() == WeatherType.UNKNOWN).count();
		
		Assert.assertEquals("All day weather must be unknown (sun outside of triangle)", days, count);
	}

	@Test
	public void testAllRain(){
		Configuration configuration = configurationService.get();
		configuration.setYears(1);
		configurationService.update(configuration);
		
		Planet betasoide = planetService.getBetasoide();
		Planet farengi = planetService.getFarengi();
		Planet vulcano = planetService.getVulcano();
		
		List<Planet> toUpdate = new ArrayList<>();
		toUpdate.add(farengi);
		toUpdate.add(betasoide);
		toUpdate.add(vulcano);

		betasoide.setCurrentAngle(135);
		betasoide.setAngularVelocity(10);

		farengi.setCurrentAngle(45);
		farengi.setAngularVelocity(10);

		vulcano.setCurrentAngle(270);
		vulcano.setAngularVelocity(10);

		planetService.update(toUpdate);

		weatherService.calculateWeather();
		
		List<Weather> weather = weatherService.getWeather();

		long days = ChronoUnit.DAYS.between(LocalDate.now(), LocalDate.now().plusYears(1));

		Assert.assertTrue("Days size error", days == weather.size());
		
		long count = weather.stream().filter(w -> w.getType() == WeatherType.RAIN).count();
		
		Assert.assertEquals("All day weather must be rain (sun inside of triangle)", days, count);
	}
	
	// private function(s)
	
	private WeatherAnalyzer getWeatherAnalyzer() {
		UnknownAnalyzerChain unknownAnalyzer = new UnknownAnalyzerChain(positionService);
		RainAnalyzerChain rainAnalyzer = new RainAnalyzerChain(positionService, planetService);
		OptimalConditionAnalyzerChain optimalAnalyzer = new OptimalConditionAnalyzerChain(positionService);
		DroughtAnalyzerChain droughtAnalyzer = new DroughtAnalyzerChain(positionService);
		
		return new WeatherAnalyzer(unknownAnalyzer, rainAnalyzer, optimalAnalyzer, droughtAnalyzer);
	}

}
