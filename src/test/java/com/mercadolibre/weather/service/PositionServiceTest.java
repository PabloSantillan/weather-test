package com.mercadolibre.weather.service;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.mercadolibre.weather.model.Planet;
import com.mercadolibre.weather.model.Point;
import com.mercadolibre.weather.repository.ConfigurationRepository;

public class PositionServiceTest {
	private static final double EPSILON = 1e-4;
	private Planet vulcano;
	private Planet betasoide;
	private Planet farengi;
	
	private PositionService positionService;
	
	@Before
	public void setup(){
		this.vulcano = new Planet("Vulvano", 10, -5, 90);
		this.betasoide = new Planet("Betasoide", 20, 3, 90);
		this.farengi = new Planet("Farengi", 5, 1, 90);
		
		this.positionService = new PositionService(new ConfigurationService(new ConfigurationRepository()));
	}
	
	@Test
	public void getVulcanoAngleAfterMovingDaysWithInitialAngleOf90(){
		Map<Integer, Double> angleByDays = new HashMap<Integer, Double>();
		angleByDays.put(0, 90d);
		angleByDays.put(5, 115d);
		angleByDays.put(36, 270d);
		angleByDays.put(54, 0d);
		angleByDays.put(72, 90d);
		angleByDays.put(90, 180d);
		angleByDays.put(180, 270d);
		
		angleByDays.forEach((day, angle) -> {
			double dayAngle = positionService.getAngleAfterMove(vulcano.getCurrentAngle(), vulcano.getAngularVelocity() * day);
			Assert.assertEquals("Error getting vulcano angle after moving " + day +" days.", angle.doubleValue(), dayAngle, EPSILON);			
		});
	}
	
	@Test
	public void getFarengiAngleAfterMovingDaysWithInitialAngleOf90(){
		Map<Integer, Double> angleByDays = new HashMap<Integer, Double>();
		angleByDays.put(0, 90d);
		angleByDays.put(10, 80d);
		angleByDays.put(90, 0d);
		angleByDays.put(180, 270d);
		angleByDays.put(270, 180d);
		angleByDays.put(360, 90d);
		angleByDays.put(450, 0d);
		angleByDays.put(540, 270d);
		angleByDays.put(720, 90d);
		angleByDays.put(2970, 0d); // (360*8) + 90
		
		angleByDays.forEach((day, angle) -> {
			double dayAngle = positionService.getAngleAfterMove(farengi.getCurrentAngle(), farengi.getAngularVelocity() * day);
			Assert.assertEquals("Error getting farengi angle after moving " + day +" days.", angle.doubleValue(), dayAngle, EPSILON);			
		});
	}

	@Test
	public void getBetasoideAngleAfterMovingDaysWithInitialAngleOf90(){
		Map<Integer, Double> angleByDays = new HashMap<Integer, Double>();
		angleByDays.put(0, 90d);
		angleByDays.put(5, 75d);
		angleByDays.put(30, 0d);
		angleByDays.put(60, 270d);
		angleByDays.put(90, 180d);
		angleByDays.put(120, 90d);
		angleByDays.put(150, 0d);
		angleByDays.put(180, 270d);
		angleByDays.put(1830, 0d); // (360*5) + 30
		
		angleByDays.forEach((day, angle) -> {
			double dayAngle = positionService.getAngleAfterMove(betasoide.getCurrentAngle(), betasoide.getAngularVelocity() * day);
			Assert.assertEquals("Error getting betasoide angle after moving " + day +" days.", angle.intValue(), dayAngle, EPSILON);			
		});
	}
	
	@Test
	public void getVulcanoPositionAfterMovingDaysWithInitialAngleOf90(){
		Map<Integer, Point> positionByDays = new HashMap<Integer, Point>();
		positionByDays.put(0, Point.from(0, 10));
		positionByDays.put(18, Point.from(-10, 0));
		positionByDays.put(36, Point.from(0, -10));
		positionByDays.put(54, Point.from(10, 0));
				
		positionByDays.forEach((day, position) -> {
			Point dayPosition = positionService.getAnglePosition(vulcano.getSunDistance(), vulcano.getCurrentAngle(), vulcano.getAngularVelocity() * day);
			Assert.assertEquals("Error getting vulcano position after moving " + day +" days.", position, dayPosition.roundedTo(5));			
		});
	}

	@Test
	public void getFarengiPositionAfterMovingDaysWithInitialAngleOf90(){
		Map<Integer, Point> positionByDays = new HashMap<Integer, Point>();
		positionByDays.put(0, Point.from(0, 5));
		positionByDays.put(90, Point.from(5, 0));
		positionByDays.put(180, Point.from(0, -5));
		positionByDays.put(270, Point.from(-5, 0));
				
		positionByDays.forEach((day, position) -> {
			Point dayPosition = positionService.getAnglePosition(farengi.getSunDistance(), farengi.getCurrentAngle(), farengi.getAngularVelocity() * day);
			Assert.assertEquals("Error getting farengi position after moving " + day +" days.", position, dayPosition.roundedTo(5));			
		});
	}

	@Test
	public void getBetasoidePositionAfterMovingDaysWithInitialAngleOf90(){
		Map<Integer, Point> positionByDays = new HashMap<Integer, Point>();
		positionByDays.put(0, Point.from(0, 20));
		positionByDays.put(30, Point.from(20, 0));
		positionByDays.put(60, Point.from(0, -20));
		positionByDays.put(90, Point.from(-20, 0));
				
		positionByDays.forEach((day, position) -> {
			Point dayPosition = positionService.getAnglePosition(betasoide.getSunDistance(), betasoide.getCurrentAngle(), betasoide.getAngularVelocity() * day);
			Assert.assertEquals("Error getting betasoide position after moving " + day +" days.", position, dayPosition.roundedTo(5));			
		});
	}
	
	@Test 
	public void areAlignedSameXAxis(){
		Point a = Point.from(0, 1);
		Point b = Point.from(0, 2);
		Point c = Point.from(0, 3);
		boolean areAligned = positionService.areAligned(a, b, c);
		
		Assert.assertTrue("Three points with same x axis must be aligned", areAligned);
	}

	@Test 
	public void areAlignedSameXAxisWithSomeDoubleValues(){
		Point a = Point.from(2.0, 1);
		Point b = Point.from(2, 2);
		Point c = Point.from(2, 3);
		boolean areAligned = positionService.areAligned(a, b, c);
		
		Assert.assertTrue("Three points with same x axis must be aligned", areAligned);
	}

	@Test 
	public void areAlignedSameXAxisWithAllDoubleValues(){
		Point a = Point.from(1.0, 1);
		Point b = Point.from(1.0000000, 2);
		Point c = Point.from(1.0000000000000000, 3);
		boolean areAligned = positionService.areAligned(a, b, c);
		
		Assert.assertTrue("Three points with same x axis must be aligned", areAligned);
	}

	@Test 
	public void areAlignedFirstTwoSameXAxis(){
		Point a = Point.from(0.0, 1);
		Point b = Point.from(0.0000000, 2);
		Point c = Point.from(5.0000000000000000, 3);
		boolean areAligned = positionService.areAligned(a, b, c);
		
		Assert.assertTrue("Three points with same x axis must not be aligned", !areAligned);
	}
	
	@Test 
	public void areAlignedLastTwoSameXAxis(){
		Point a = Point.from(1.0, 1);
		Point b = Point.from(2.0000000, 2);
		Point c = Point.from(2.0000000000000000, 3);
		boolean areAligned = positionService.areAligned(a, b, c);
		
		Assert.assertTrue("Three points with same x axis must not be aligned", !areAligned);
	}

	@Test 
	public void areAlignedToOrigin(){
		Point a = Point.from(0, 1);
		Point b = Point.from(0.0000000, 2);
		Point c = Point.from(0.0000000000000000, 3);
		boolean areAligned = positionService.areAlignedToOrigin(a, b, c);
		
		Assert.assertTrue("Three points with same x axis = 0 must be aligned to origin", areAligned);
	}

	@Test 
	public void areAlignedToOriginDiffPoints(){
		Point a = Point.from(2, 1);
		Point b = Point.from(4, 2);
		Point c = Point.from(8, 4);
		boolean areAligned = positionService.areAlignedToOrigin(a, b, c);
		
		Assert.assertTrue("Three diff points must be aligned to origin", areAligned);
	}
	
	@Test 
	public void areNotAlignedToOrigin(){
		Point a = Point.from(1, 1);
		Point b = Point.from(1.0000000, 2);
		Point c = Point.from(1.0000000000000000, 3);
		boolean areAligned = positionService.areAlignedToOrigin(a, b, c);
		
		Assert.assertTrue("Three points with same x axis = 1 must be not aligned to origin", !areAligned);
	}

	@Test 
	public void pointIsInsideTriangle(){
		Point p = Point.from(0, 0);
		Point v1 = Point.from(2, 0);
		Point v2 = Point.from(0, -2);
		Point v3 = Point.from(-2, 2);
		boolean inside = positionService.isPointInsideTriangle(v1, v2, v3, p);
		
		Assert.assertTrue("Point must be inside of triangle", inside);
	}

	@Test 
	public void pointIsInsideTriangleFirstQuadrant(){
		Point p = Point.from(1.5, 2.5);
		Point v1 = Point.from(2, 0);
		Point v2 = Point.from(0, 3);
		Point v3 = Point.from(2, 3);
		boolean inside = positionService.isPointInsideTriangle(v1, v2, v3, p);
		
		Assert.assertTrue("Point must be inside of triangle - 1rt cuadrant", inside);
	}

	@Test 
	public void pointIsInsideTriangleSecondQuadrant(){
		Point p = Point.from(2.3, -0.8);
		Point v1 = Point.from(1, 0);
		Point v2 = Point.from(0, -4);
		Point v3 = Point.from(8, 0);
		boolean inside = positionService.isPointInsideTriangle(v1, v2, v3, p);
		
		Assert.assertTrue("Point must be inside of triangle - 2nd cuadrant", inside);
	}

	@Test 
	public void pointIsInsideTriangleThirdQuadrant(){
		Point p = Point.from(-4.6, -2.453);
		Point v1 = Point.from(-3, -1);
		Point v2 = Point.from(-8, -3);
		Point v3 = Point.from(-3, -5);
		boolean inside = positionService.isPointInsideTriangle(v1, v2, v3, p);
		
		Assert.assertTrue("Point must be inside of triangle - 3th cuadrant", inside);
	}

	@Test 
	public void pointIsInsideTriangleFourthQuadrant(){
		Point p = Point.from(-4.6, 1.765);
		Point v1 = Point.from(-3, 1);
		Point v2 = Point.from(-8, 3);
		Point v3 = Point.from(-3, 5);
		boolean inside = positionService.isPointInsideTriangle(v1, v2, v3, p);
		
		Assert.assertTrue("Point must be inside of triangle - 4th cuadrant", inside);
	}

	@Test 
	public void pointNotInsideTriangle(){
		Point p = Point.from(0, 0);
		Point v1 = Point.from(2, 0);
		Point v2 = Point.from(0, 2);
		Point v3 = Point.from(3, 3);
		boolean inside = positionService.isPointInsideTriangle(v1, v2, v3, p);
		
		Assert.assertTrue("Point is not inside of triangle", !inside);
	}
	
	@Test
	public void maxPerimeter(){
		double max = positionService.maxPerimeterByRadius(3, 5, 7);
		Assert.assertEquals("max perimeter incorrect. ", 26.47085, max, EPSILON);
	}

	@Test
	public void NaN(){
		double a = 1d;
		double b = 0;
		double c = a / b;

		Assert.assertEquals(Double.POSITIVE_INFINITY, c, EPSILON);
	}
}
